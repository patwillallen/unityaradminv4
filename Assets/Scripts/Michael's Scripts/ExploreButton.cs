﻿using UnityEngine;
using System.Collections;

public class ExploreButton : MonoBehaviour 
{
	private GameObject options;
	
	public string modelName;
	public bool beingExplored;
	
	void Awake ()
	{
		options = GameObject.FindGameObjectWithTag("Options");
	}
	
	void OnGUI()
	{
		if (GUI.Button(new Rect((Screen.width/6)*5, 0, Screen.width/6, Screen.height/8), "Explore"))
		{
				options.GetComponent<Options>().modelName = modelName;
				Application.LoadLevel("Scene2");
		}
	}
}
