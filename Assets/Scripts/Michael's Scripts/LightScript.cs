﻿using UnityEngine;
using System.Collections;

public class LightScript : MonoBehaviour {

	private Texture2D sliderBackground;
	private Options options;
	private GUISkin guiSkin;
	
	void Awake()
	{
		gameObject.light.intensity = 0.7f;
		options = GameObject.FindGameObjectWithTag("Options").GetComponent<Options>();
		guiSkin = options.guiSkin;
		StartCoroutine(Example (options.GetComponent<Options>().startURL+"/Textures/Slider_Background.png"));
	}
	
	IEnumerator Example(string filePath) 
	{	
		WWW www;
		if (Application.isEditor)
		{
			www = new WWW(filePath);
		}
		else
		{
			www = new WWW(filePath);
		}
		yield return www;
		sliderBackground = www.texture;
	}
	
	// Update is called once per frame
	void OnGUI () 
	{
		gameObject.light.intensity = GUI.VerticalSlider(new Rect(Screen.width-Screen.width/12, (Screen.height/5)*4, Screen.width/24, (Screen.height/5)), gameObject.light.intensity, 2.0F, 0.0F,guiSkin.verticalSlider,guiSkin.verticalSliderThumb);
		//GUI.Label (new Rect(Screen.width/4, Screen.height-Screen.height/8,Screen.width/2, Screen.height/16 ),gameObject.light.intensity.ToString());
	}
}
