﻿using UnityEngine;
using System.Collections;

public class ZoomScript : MonoBehaviour {

	public GUISkin guiSkin;
	private bool zoomIn;
	private bool zoomOut;
	
	public Texture2D [] icons;
	public float speed = 2f;
	private Vector3 origionalSize;
	private Vector3 scale;
	
	private float zoomInLimit = 2.0f;
	private float zoomOutLimit = 0.7f;
	private Options options;
	
	void Start()
	{
		options = GameObject.FindWithTag("Options").GetComponent<Options>();
		guiSkin = options.guiSkin;
		icons = new Texture2D[2];
		StartCoroutine(Example (options.startURL+"/Textures/ZoomIn.png",0));
		StartCoroutine(Example (options.startURL+"/Textures/ZoomOut.png",1));
		
		scale = transform.localScale;
		origionalSize = new Vector3 ();
		origionalSize = scale;
	}
	
	IEnumerator Example(string filePath, int iterator) 
	{	
		WWW www;
		if (Application.isEditor)
		{
			www = new WWW(filePath);
		}
		else
		{
			www = new WWW(filePath);
		}
		yield return www;
		icons[iterator] = www.texture;
	}
	
	void OnGUI()
	{	
		if (GUI.RepeatButton(new Rect((Screen.width/14)*13f, (Screen.height/36), Screen.width/14, (Screen.height/14)*1.6f), icons[0],guiSkin.button))
		{
			zoomIn = true;
		}
		else
		{
			zoomIn = false;
		}
		if (GUI.RepeatButton(new Rect((Screen.width/14)*11f, (Screen.height/36), Screen.width/14, (Screen.height/14)*1.6f), icons[1],guiSkin.button))
		{
			zoomOut = true;
		}
		else
		{
			zoomOut = false;
		}
	}
	
	void FixedUpdate()
	{
		if (zoomIn)
		{
			transform.localPosition = new Vector3 (transform.position.x,transform.position.y, transform.position.z-10*Time.deltaTime);
		}
		else if (zoomOut)
		{
			transform.localPosition = new Vector3 (transform.position.x,transform.position.y, transform.position.z+10*Time.deltaTime);
		}
	}
}
