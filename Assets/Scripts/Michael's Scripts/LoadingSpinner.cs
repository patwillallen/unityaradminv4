﻿using UnityEngine;
using System.Collections;

public class LoadingSpinner : MonoBehaviour 
{	
	public string url;
	public Options options;
	void Awake()
	{
		options = GameObject.FindGameObjectWithTag("Options").GetComponent<Options>();
		if (Application.isEditor)
		{
			url = options.startURL+"/Textures/Preloader_Solid.png";
		}
		else
		{
			url = options.startURL+"Textures/Preloader_Solid.png";
		}
	}
	
	IEnumerator Start()
	{
		WWW www = new WWW(url);
		yield return www;
		
		renderer.material.mainTexture = www.texture;
		renderer.material.shader = Shader.Find("Transparent/Diffuse");
	}
	
	void Update()
	{
		transform.Rotate(Vector3.back * 40*Time.deltaTime);
	}
}
