﻿using UnityEngine;
using System.Collections;

public class Options : MonoBehaviour 
{
	/*Explore Variables*/
	public string modelName;
	/*RotationVariables*/
	public float rotateSensitivity;//used to dictate the speed of the objects' rotation.
	/*General Game Variables*/
	public bool syncToServer;//should the application always sync to server?
	public bool autoLogin;//should the application automatically log the user in based on the previously saved username and password?
	public string username;//the current saved username.
	public string password;//the current saved password.
	public string course;//which course is the user on.
	/*Cosmetic Variables*/
	public GUISkin guiSkin;
	/*Course Variables*/
	public int courseNum;
	public int figureNum;
	public string figureName;
	public bool isTest;
	public bool admin;
	public string startURL;
	
	void Awake()
	{
		SetGUISkin();
		DontDestroyOnLoad(transform.gameObject);
		ReadFromDocument();
		if (Application.isEditor)
		{
			startURL = "file:///"+Application.streamingAssetsPath;
			print ("Start URL: "+startURL);
		}
		else
		{
			startURL = gameObject.GetComponent<OptionsFunctions>().FindLocation();
		}
	}
	
	public void	SaveToDocument()//saves the variables in options to a .txt document in StreamingAssets
	{
		//gameObject.GetComponent<SaveOptions>().WriteToFile();
	}
	public void ReadFromDocument()
	{
		//gameObject.GetComponent<OptionsFunctions>().RetrieveOptions();
	}
	public void SetGUISkin()
	{
		gameObject.GetComponent<OptionsFunctions>().FindGUISettings();	
	}
}
