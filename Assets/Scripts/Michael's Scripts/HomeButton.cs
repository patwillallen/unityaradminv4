﻿using UnityEngine;
using System.Collections;

public class HomeButton : MonoBehaviour {

	public GUISkin guiSkin;
	private Texture2D icon;
	
	void Start()
	{
		guiSkin = Resources.Load ("ButtonSkin") as GUISkin;
		icon = Resources.Load ("Materials/home") as Texture2D;
	}
	void OnGUI()
	{
		if (GUI.RepeatButton(new Rect((Screen.width/14)*12f, (Screen.height/36), Screen.width/14, (Screen.height/14)*1.6f), icon,guiSkin.button))
		{
			transform.localRotation = new Quaternion (0,0,0,0);
			transform.localPosition = new Vector3 (0,0,0);
			transform.localScale = new Vector3 (1,1,1);
		}
	}
}
