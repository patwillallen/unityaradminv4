﻿using UnityEngine;
using System.Collections;

public class WebSpawnerScript : MonoBehaviour {

	private bool spinnerActive;//is the spinning loading thing active?
	public int course;
	public int figure = 1;
	
	private string debug;
	public Options options;
	public Texture2D loadingIcon;
	private bool loading;
	private string url;
	//string baseUrl = Request.Url.GetLeftPart(UriPartial.Authority); 
	
	void Awake()
	{options = GameObject.FindWithTag("Options").GetComponent<Options>();}
	void Start()
	{
		
		Caching.CleanCache();
		loading = false;
		LoadModel(options.modelName);
		
	}
	public void LoadModel(string name)
	{
		course = options.courseNum;
		figure = options.figureNum;
		
		if (Application.isEditor)
		{
			url = options.startURL+"/AssetBundles/Course"+options.courseNum.ToString()+"/"+options.figureName+"/"+options.figureName+"";
			print ("if suddently models won't load from the server, check here first");
		}
		else
		{
			url = options.startURL+"AssetBundles/Course"+options.courseNum.ToString()+"/"+options.figureName+"/"+options.figureName+"";
		}
		if (options.isTest)
			url += "_Test";
		else
			url += "_Solution";
		print ("URL: "+url);
		//print ("url: "+url);
		//print ("Base URL: "+baseUrl);
		if (Application.isEditor)
		{
			url += ".unity3d";
		}
		else
		{
			url += ".unity3d";
		}

		StartCoroutine(ModelLoader(name));
	}
	
	IEnumerator ModelLoader(string modelName)
	{
		
		
		loading = true;
		WWW www = WWW.LoadFromCacheOrDownload(url, 1);
		
		
		
		//Wait for download to complete
		yield return www;//this is never passed, problem here.
		
		//Load and retrieve the asset bundle
		AssetBundle bundle = www.assetBundle;
	
		GameObject go;
		//Load the GameObject(s) 
		if (options.isTest)
		{
			go = bundle.Load(options.figureName+"_Test", typeof(GameObject)) as GameObject;
		}
		else
		{
			go = bundle.Load(options.figureName+"_Solution", typeof(GameObject)) as GameObject;
		}
		
		//Instantiate the GameObjects
		GameObject goCopy = (GameObject)Instantiate (go) as GameObject;
		
		//Do whatever you want with the object(s)
		//goCopy.transform.parent=transform;
		
		goCopy.transform.tag = "theModel";		
		goCopy.AddComponent<PlaceTarget>();
		goCopy.AddComponent<PanningScript>();
		
		//goCopy.AddComponent<WriteFault>();
		//goCopy.AddComponent<CoordDisplay>();
		goCopy.AddComponent<HomeLocation>();
		
		//goCopy.AddComponent<HomeButton>();
		goCopy.AddComponent<ButtonRotationScript>();
		goCopy.AddComponent<ZoomScript>();
		
		goCopy.transform.localScale = new Vector3 (1f,1f,1f);
		goCopy.transform.localPosition = new Vector3 (0.0f,0.0f,0.0f);
		
		bundle.Unload(false);
		
		loading = false;
	}
	
	void Update()
	{
		if (loading && !spinnerActive)
		{
			Instantiate(Resources.Load ("Spinner") as GameObject, new Vector3(0, 0, 0), Quaternion.identity);
			spinnerActive = true;
		}
		else if (!loading && spinnerActive)
		{
			GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Spinner");
			foreach (GameObject target in gameObjects) {
				GameObject.Destroy(target);
			}
			spinnerActive = false;
		}
		print (options.courseNum);
	}
}
