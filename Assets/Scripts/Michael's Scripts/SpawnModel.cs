﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class SpawnModel : MonoBehaviour {

	private string modelBase64;
	
	public MyTrackableEventHandler eventHandler;

	public string modelName;

	public bool canBeActedOn;
	public bool modelSpawned;

	public GameObject go;

	void Awake()
	{
		canBeActedOn = false;
		modelSpawned = false;
	}

	// Update is called once per frame
	void Update () 
	{
		modelName = eventHandler.trackableName;
		if (!modelSpawned) 
		{
			Spawn ();
		} 	
	}

	void Spawn()
	{
		if (modelName == "") 
		{
			modelSpawned = false;	
		}
		else
		{
			modelSpawned = true;
		}
	}
	
}
