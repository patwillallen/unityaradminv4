﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class HomeLocation : MonoBehaviour 
{
	private bool initialSetupCompleted = false; 
	private string url;
	private string rawText;
	private List<string> lines = new List<string>();
	private int selectedFault = 0;
	private GUISkin guiSkin;
	private Texture2D icon;
	private string infoString;
	private Options options;
	
	void Awake()
	{
		options = GameObject.FindWithTag("Options").GetComponent<Options>();
		guiSkin = options.GetComponent<Options>().guiSkin;
		StartCoroutine(Example (options.startURL+"/Textures/home.png"));
		if (Application.isEditor)
			url = options.startURL+"/TextFolder/Home.txt";
		else
			url = options.startURL+"/TextFolder/Home.txt";
		StartCoroutine(FindAsset ());
	}
	IEnumerator Example(string filePath) 
	{	
		WWW www;
		if (Application.isEditor)
		{
			www = new WWW(filePath);
		}
		else
		{
			www = new WWW(filePath);
		}
		yield return www;
		icon = www.texture;
	}
	/*Get TextDocument*/
	IEnumerator FindAsset()
	{
		WWW www = new WWW(url);
		
		//Wait for download to complete
		yield return www;
		/*Get Text*/
		//Load and retrieve the TextAsset
		rawText = www.text;
		ConvertToLines();
	}
	/*Divide Text by line (\n)*/
	void ConvertToLines()
	{
		string str = null;
		string[] strArr = null;
		int count = 0;
		str = rawText;
		char[] splitchar = { '\n' };
		strArr = str.Split(splitchar);
		/*Store each line in an List*/
		/*Each line represents 1 fault*/
		for (int i = 1; i <= strArr.Length-1; i++ )
		{
			lines.Insert(i-1, strArr[i]); 
		}
		GoHome ();
	}
	
	/*The user specifies which fault they want*/
	void OnGUI()
	{
		/*If the user wants to view the fault*/
		if (GUI.RepeatButton(new Rect((Screen.width/14)*12.0f, (Screen.height/36), Screen.width/13, (Screen.height/13)*1.6f), icon,guiSkin.button))
		{
			GoHome ();
		}
		if (options.admin)
		{
			if (GUI.Button(new Rect(100,60,40,20),"Set Home"))
			{
				ConvertVectorToString();
				OverwriteDocument();
				FindAsset();
			}
		}
	}	
	
	private void GoHome()
	{
		initialSetupCompleted = true;
		string str = null;
		string[] strArr = null;
		int count = 0;
		/*Seperate line by "/" to find each value of the fault*/
		str = lines[0];
		print ("lines[0]: "+lines[0]);
		char[] splitchar = { '/' };
		strArr = str.Split(splitchar);
		/*Apply those values to the current model*/		
		float [] rotationFloats = new float[strArr.Length];
		float [] positionFloats = new float[strArr.Length];
		for (int i = 0; i <= strArr.Length-1; i++ )
		{
			if (i > -1 && i < 3)
				rotationFloats[i] = float.Parse (strArr[i]);
			else if (i >= 3 && i < 6)
				positionFloats[i-3] = float.Parse (strArr[i]);
		}
		transform.position = new Vector3 (positionFloats[0],positionFloats[1],positionFloats[2]);
		transform.rotation = Quaternion.Euler(rotationFloats[0],rotationFloats[1],rotationFloats[2]);
		//transform.rotation = new Quaternion (rotationFloats[0],rotationFloats[1],rotationFloats[2],transform.localRotation.w);
		
	}
	
	private void ConvertVectorToString()
	{
		infoString = "";
		infoString = infoString + "Rotation.x/Rotation.y/Rotation.z/Position.x/Position.y/Position.z\n";
		infoString = infoString + (transform.rotation.eulerAngles.x.ToString());
		infoString = infoString + "/";
		infoString = infoString + (transform.rotation.eulerAngles.y.ToString());
		infoString = infoString + "/";
		infoString = infoString + (transform.rotation.eulerAngles.z.ToString());
		infoString = infoString + "/";
		infoString = infoString + (transform.position.x.ToString());
		infoString = infoString + "/";
		infoString = infoString + transform.position.y.ToString ();
		infoString = infoString + "/";
		infoString = infoString + transform.position.z.ToString ();
	}
	private void OverwriteDocument()
	{	
		string theurl = "";
		if (Application.isEditor)
			theurl = options.startURL+"/TextFolder/Home.txt";
		else
			theurl = options.startURL+"/TextFolder/Home.txt";
			
		StreamWriter writer = new StreamWriter(theurl);
		writer.WriteLine(infoString);
		writer.Close();
		
	}
}
