﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WriteFault : MonoBehaviour 
{
	/*Get TextDocument*/
	/*Get Text*/
	/*Divide Text by line (\n)*/
	/*Store each line in an List*/
	/*Each line represents 1 fault*/
	/*The user specifies which fault they want*/
	/*If the user wants to view the fault*/
		/*Seperate line by "/" to find each value of the fault*/
		/*Apply those values to the current model*/
	/*If the user wants to edit a fault*/
		/*Seperate line by "/" to find each value of the fault*/
		/*Replace these values with the values of the current model*/
		/*Overwrite .txt with new information*/
	/*If the user wants to add a new fault*/
		/*Add a new line to the line List with the model's current values as it's values*/
		/*Overwrite .txt with new information*/
	
	private string url;
	private string rawText;
	private List<string> lines = new List<string>();
	private int selectedFault = 0;
	private GUISkin guiSkin;
	
	void Awake()
	{
		guiSkin = Resources.Load ("ButtonSkin") as GUISkin;
		if (Application.isEditor)
			url = "file:///"+Application.streamingAssetsPath+"/Faults/Figure1.txt";
		else
			url = Application.streamingAssetsPath+"/Faults/Figure1.txt";
		StartCoroutine(FindAsset ());
	}
/*Get TextDocument*/
	IEnumerator FindAsset()
	{
		WWW www = new WWW(url);
		
		//Wait for download to complete
		yield return www;
/*Get Text*/
		//Load and retrieve the TextAsset
		rawText = www.text;
		ConvertToLines();
	}
/*Divide Text by line (\n)*/
	void ConvertToLines()
	{
		string str = null;
		string[] strArr = null;
		int count = 0;
		str = rawText;
		char[] splitchar = { '\n' };
		strArr = str.Split(splitchar);
/*Store each line in an List*/
/*Each line represents 1 fault*/
		for (int i = 1; i <= strArr.Length-1; i++ )
		{
			lines.Insert(i-1, strArr[i]); 
		}
	}
	
/*The user specifies which fault they want*/
	void OnGUI()
	{
		if (GUI.Button(new Rect(10,10,40,20),"<"))
		{
			if (selectedFault > 0)
				selectedFault --;
		}
		GUI.Label (new Rect(50, 10, 60,20),"Fault "+(selectedFault+1),guiSkin.label);
		if (GUI.Button(new Rect(110,10,40,20),">"))
		{
			if (selectedFault < lines.Count-1)
				selectedFault ++;
		}
/*If the user wants to view the fault*/
		if (GUI.Button(new Rect(10,30,40,20),"View Fault"))
		{
			string str = null;
			string[] strArr = null;
			int count = 0;
/*Seperate line by "/" to find each value of the fault*/
			str = lines[selectedFault];
			char[] splitchar = { '/' };
			strArr = str.Split(splitchar);
/*Apply those values to the current model*/		
			float [] rotationFloats = new float[strArr.Length];
			float [] positionFloats = new float[strArr.Length];
			for (int i = 0; i <= strArr.Length-1; i++ )
			{
				if (i > -1 && i < 3)
					rotationFloats[i] = float.Parse (strArr[i]);
				else if (i >= 3 && i < 6)
					positionFloats[i] = float.Parse (strArr[i]);
			}
			transform.position = new Vector3 (positionFloats[0],positionFloats[1],positionFloats[2]);
			transform.localRotation = new Quaternion (rotationFloats[0],rotationFloats[1],rotationFloats[2],transform.localRotation.w);
		}
	}	
	
	void Update()
	{
		for (int i = 0; i <= lines.Count-1; i++)
		{
			print (lines.Count);
		}
		
	}
	
}
