﻿using UnityEngine;
using System.Collections;

public class PanningScript : MonoBehaviour {

	public GUISkin guiSkin;
	public Texture2D[] arrows;
	public GameObject options;
	
	private bool useButtons;
	private bool useRepeatButtons;
	public float degrees;
	
	private bool up;
	private bool down;
	private bool right;
	private bool left;
	
	
	void Awake()
	{
		options = GameObject.FindGameObjectWithTag("Options");
		guiSkin = options.GetComponent<Options>().guiSkin;
		arrows = new Texture2D[4];
		StartCoroutine(Example (options.GetComponent<Options>().startURL+"/Textures/panUp.png",0));
		StartCoroutine(Example (options.GetComponent<Options>().startURL+"/Textures/panDown.png",1));
		StartCoroutine(Example (options.GetComponent<Options>().startURL+"/Textures/panLeft.png",2));
		StartCoroutine(Example (options.GetComponent<Options>().startURL+"/Textures/panRight.png",3));
		
		
	}
	
	IEnumerator Example(string filePath, int iterator) 
	{	
		WWW www;
		if (Application.isEditor)
		{
			www = new WWW(filePath);
		}
		else
		{
			www = new WWW(filePath);
		}
		yield return www;
		arrows[iterator] = www.texture;
	}
	
	void Start()
	{
		degrees = (options.GetComponent<Options>().rotateSensitivity);
	}
	
	void OnGUI()
	{
		
		if (GUI.RepeatButton(new Rect((Screen.width/14)*11f, (Screen.height/42)*10.8125f, (Screen.width/14)*1f, (Screen.height/14)*1.6f), arrows[2],guiSkin.button))
			left = true;
		else
			left = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/14)*13f, (Screen.height/42)*10.8125f, (Screen.width/14)*1f, (Screen.height/14)*1.6f), arrows[3],guiSkin.button))
			right = true;
		else
			right = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/14)*12f, (Screen.height/42)*7.75f, (Screen.width/14)*1f, (Screen.height/14)*1.6f), arrows[0],guiSkin.button))
			up = true;
		else
			up = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/14)*12f, Screen.height/42*(13f),(Screen.width/14)*1f, (Screen.height/14)*1.6f), arrows[1],guiSkin.button))
			down = true;
		else
			down = false;
		
	}
	
	
	// Update is called once per frame
	void Update () 
	{
		
		/*Apply Movement hold down*/
		if (up)
			gameObject.transform.Translate(Vector3.up* 20.0f *Time.deltaTime, Space.World);
		if (down)
			gameObject.transform.Translate(Vector3.down* 20.0f *Time.deltaTime, Space.World);
		if (left)
			gameObject.transform.Translate(Vector3.left* 20.0f *Time.deltaTime, Space.World);
		if (right)
			gameObject.transform.Translate(Vector3.right* 20.0f *Time.deltaTime, Space.World);
		
	}
}
