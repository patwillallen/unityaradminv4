﻿using UnityEngine;
using System.Collections;

public class CoordDisplay : MonoBehaviour 
{
	private string [] stringToEdit   = {"0", "0", "0"};
	private string [] rotationLabels = {"0", "0", "0"};
	private float  [] nums 			 = { 0f,  0f,  0f};
	private bool canMove = true;
	
	
	
	void OnGUI()
	{
		rotationLabels[0] = (transform.localRotation.x * 180).ToString ();
		rotationLabels[1] = (transform.localRotation.y * 180).ToString ();
		rotationLabels[2] = (transform.localRotation.z * 180).ToString ();
		
		/*Creates the Editable text feilds*/
		stringToEdit[0] = GUI.TextField(new Rect(10, 090, 40, 20), stringToEdit[0], 25);
		stringToEdit[1] = GUI.TextField(new Rect(10, 110, 40, 20), stringToEdit[1], 25);
		stringToEdit[2] = GUI.TextField(new Rect(10, 130, 40, 20), stringToEdit[2], 25);
		
		/*Creates Uneditable Labels Displaying the current Rotation*/
		GUI.Label(new Rect(30, 090, 40, 20), rotationLabels[0]);
		GUI.Label(new Rect(30, 110, 40, 20), rotationLabels[1]);
		GUI.Label(new Rect(30, 130, 40, 20), rotationLabels[2]);
		
		
		/*Use a for-loop to check all values in stringToEdit, if something is incorrect then set canMove = false;*/
		canMove = true;
		for (int i = stringToEdit.Length-1; i >= 0; i--)
		{
			if (float.TryParse(stringToEdit[i], out nums[i])) {}
			else 
				canMove = false;  
		}
		
		/*If this button is pressed and canMove is true, rotate the object according to the user's values.*/
		if (	GUI.Button(new Rect(10,150,40,20), "")	)
		{
			if (canMove)
				transform.localRotation = Quaternion.Euler(nums[0], nums[1], nums[2]);
		}
	}
}
