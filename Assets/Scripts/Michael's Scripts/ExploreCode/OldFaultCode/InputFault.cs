﻿using UnityEngine;
using System.Collections;
using System.IO;

public class InputFault : MonoBehaviour 
{
	private Vector4 info;
	private Vector3 infoPos;
	private string infoString;
	public void Input()
	{
		GrabVector ();
		ConvertVectorToString();
		StartCoroutine(OverwriteDocument());
	}
	private void GrabVector()
	{
		info = gameObject.GetComponent<FindFault>().savedFault;
		infoPos = gameObject.GetComponent<FindFault>().faultPosition;
	}
	private void ConvertVectorToString()
	{
		infoString = "";
		infoString = infoString + (info[0].ToString());
		infoString = infoString + ",";
		infoString = infoString + (info[1].ToString());
		infoString = infoString + ",";
		infoString = infoString + (info[2].ToString());
		infoString = infoString + ",";
		infoString = infoString + (info[3].ToString());
		infoString = infoString + ",";
		infoString = infoString + infoPos[0].ToString ();
		infoString = infoString + ",";
		infoString = infoString + infoPos[1].ToString ();
		infoString = infoString + ",";
		infoString = infoString + infoPos[2].ToString ();
		
	}
	private IEnumerator OverwriteDocument()
	{
		string url = "";
		print ("URL Test: "+url);
		if (Application.isEditor)
			url = "Assets/StreamingAssets/Faults/Figure1.txt";
		else
			url = Application.streamingAssetsPath+"/Faults/Figure1.txt";
		print ("URL Test2: "+url);
		WWW www = new WWW(url);
		print ("URL Test3: "+url);
		//Wait for download to complete
		yield return www;
		print ("URL Test4: "+url);
		
		StreamWriter writer = new StreamWriter(url);
		print ("URL Test5: "+url);
		writer.WriteLine(infoString);
		writer.Close();
		
	}
}
