﻿using UnityEngine;
using System.Collections;

public class FindFault : MonoBehaviour 
{
	public Vector4 savedInfo;
	public Vector4 savedFault;
	public Vector3 savedPosition;
	public Vector3 faultPosition;
	public Vector3 objRotation;
	public Vector3 objPosition;
	public float objScale;
	void Start()
	{
		savedInfo = new Vector4(gameObject.transform.rotation.x,gameObject.transform.rotation.y,gameObject.transform.rotation.z,objScale);
		objRotation = new Vector3(gameObject.transform.rotation.x,gameObject.transform.rotation.y,gameObject.transform.rotation.z);
		
		objScale = gameObject.transform.localScale.x;	
	}
	// Update is called once per frame
	void FixedUpdate () 
	{
		//objRotation = new Vector3(gameObject.transform.rotation.x,gameObject.transform.rotation.y,gameObject.transform.rotation.z);
		objRotation = new Vector3(gameObject.transform.rotation.eulerAngles.x,gameObject.transform.rotation.eulerAngles.y, gameObject.transform.rotation.eulerAngles.z);
		
		objScale = gameObject.transform.localScale.x;	
	}
	
	void OnGUI()
	{
		/*
		//Current Info
		GUI.Label(new Rect(50,100,200,60),"Rotation: "+objRotation.x+"\n"+objRotation.y+"\n"+objRotation.z);
		
		GUI.Label(new Rect(50,160,200,40),"Scale: "+objScale+"%");
		GUI.Label (new Rect(50,180,200,40),"Co-ordinates: "+gameObject.transform.position.x+","+gameObject.transform.position.y+","+gameObject.transform.position.z);
		
		//Local Temp Saved Info
		GUI.Label(new Rect(250,100,200,60),"Saved Rotation: "+savedInfo.x+"\n"+savedInfo.y+"\n"+savedInfo.z);
		GUI.Label(new Rect(250,160,200,40),"Saved Scale: "+savedInfo.w+"%");
		GUI.Label (new Rect(250,180,200,40),"Saved Co-ordinates: "+savedPosition.x+","+savedPosition.y+","+savedPosition.z);
		
		
		//Saved to Server Info
		GUI.Label(new Rect(450,100,200,60),"Fault Rotation: "+savedFault.x+"\n"+savedFault.y+"\n"+savedFault.z);
		GUI.Label(new Rect(450,160,200,40),"Fault Scale: "+savedFault.w+"%");
		GUI.Label (new Rect(450,180,200,40),"Fault Co-ordinates: "+faultPosition.x+","+faultPosition.y+","+faultPosition.z);
		*/
		/*
		if (GUI.Button(new Rect(0,0,Screen.width/12, Screen.height/16),"Save"))
		{
			savedInfo = new Vector4(objRotation.x,objRotation.y,objRotation.z,objScale);
			savedPosition = gameObject.transform.position;
		}
		if (GUI.Button(new Rect(0,Screen.height/16,Screen.width/12, Screen.height/16),"Snap"))
		{
			//gameObject.transform.localRotation = new Vector3(savedInfo.x, savedInfo.y, savedInfo.z);
			gameObject.transform.rotation = Quaternion.Euler(savedInfo.x, savedInfo.y, savedInfo.z);
			gameObject.transform.localScale = new Vector3(savedInfo.w,savedInfo.w,savedInfo.w);
			gameObject.transform.position = savedPosition;
		}
		*/
		if (GUI.Button(new Rect(0,Screen.height/8,Screen.width/12, Screen.height/16),"AddFault"))
		{
			savedFault = new Vector4(objRotation.x,objRotation.y,objRotation.z,objScale);
			faultPosition = gameObject.transform.position;
			MoveCrosshair();
			InputFault();
		}
		if (GUI.Button(new Rect(0,(Screen.height/16)*3,Screen.width/12, Screen.height/16),"FindFault"))
		{
			//gameObject.transform.localRotation = new Vector3(savedInfo.x, savedInfo.y, savedInfo.z);
			gameObject.transform.rotation = Quaternion.Euler(savedFault.x, savedFault.y, savedFault.z);
			gameObject.transform.localScale = new Vector3(savedFault.w,savedFault.w,savedFault.w);
			gameObject.transform.position = faultPosition;
			MoveCrosshair ();
		}
		
	}
	
	void InputFault()
	{
		gameObject.GetComponent<InputFault>().Input();
	}
	void MoveCrosshair()
	{
		gameObject.GetComponent<PlaceTarget>().CreateCrosshair();
	}
}