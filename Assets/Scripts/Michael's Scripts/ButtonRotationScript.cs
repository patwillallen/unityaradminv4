﻿using UnityEngine;
using System.Collections;

public class ButtonRotationScript : MonoBehaviour {
	
	public GUISkin guiSkin;
	public Texture2D[] arrows;
	public GameObject options;
	
	public float degrees;
	
	private bool up;
	private bool down;
	private bool right;
	private bool left;
	
	void Awake()
	{
		options = GameObject.FindGameObjectWithTag("Options");
		guiSkin = options.GetComponent<Options>().guiSkin;
		arrows = new Texture2D[4];
		StartCoroutine(Example (options.GetComponent<Options>().startURL+"/Textures/tiltUp.png",0));
		StartCoroutine(Example (options.GetComponent<Options>().startURL+"/Textures/tiltDown.png",1));
		StartCoroutine(Example (options.GetComponent<Options>().startURL+"/Textures/tiltLeft.png",2));
		StartCoroutine(Example (options.GetComponent<Options>().startURL+"/Textures/tiltRight.png",3));
	}
	
	IEnumerator Example(string filePath, int iterator) 
	{	
		WWW www;
		if (Application.isEditor)
		{
			www = new WWW(filePath);
		}
		else
		{
			www = new WWW(filePath);
		}
		yield return www;
		arrows[iterator] = www.texture;
	}
		
	
	void Start()
	{
		degrees = (options.GetComponent<Options>().rotateSensitivity);
	}
	
	void OnGUI()
	{
		if (GUI.RepeatButton(new Rect((Screen.width/14)*11f, (Screen.height/42)*23.8125f, (Screen.width/14)*1f, (Screen.height/14)*1.6f), arrows[2],guiSkin.button))
			left = true;
		else
			left = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/14)*13f, (Screen.height/42)*23.8125f, (Screen.width/14)*1f, (Screen.height/14)*1.6f), arrows[3],guiSkin.button))
			right = true;
		else
			right = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/14)*12f, (Screen.height/42)*20.75f, (Screen.width/14)*1f, (Screen.height/14)*1.6f), arrows[0],guiSkin.button))
			up = true;
		else
			up = false;
		
		if (GUI.RepeatButton(new Rect((Screen.width/14)*12f, Screen.height/42*(26f), (Screen.width/14)*1f, (Screen.height/14)*1.6f), arrows[1],guiSkin.button))
			down = true;
		else
			down = false;
	}
	
	
	// Update is called once per frame
	void Update () 
	{
		/*Apply Movement hold down*/
		if (up)
			gameObject.transform.RotateAround(gameObject.transform.position, Vector3.right, degrees*Time.deltaTime);
		if (down)
			gameObject.transform.RotateAround(gameObject.transform.position, Vector3.left, degrees*Time.deltaTime);
		if (left)
			gameObject.transform.RotateAround(gameObject.transform.position, Vector3.up, degrees*Time.deltaTime);
		if (right)
			gameObject.transform.RotateAround(gameObject.transform.position, Vector3.down, degrees*Time.deltaTime);
	}
}
